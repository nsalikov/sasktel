# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html


from scrapy.exceptions import DropItem


class SasktelPipeline(object):

    def process_item(self, item, spider):
        item['Authorized'] = 'Yes' if ('Dealer' in item and 'Authorized' in item['Dealer']) else 'No'
        item['Name'] = item['Name'].replace('&nbsp;', ' ').replace('&#39;', "'").replace('&amp;', '&').strip()
        item['Address'] = item['Address'].replace('&nbsp;', ' ').replace('&#39;', "'").replace('&amp;', '&').strip()

        return item


class DuplicatesPipeline(object):

    def __init__(self):
        self.seen = set()


    def process_item(self, item, spider):
        return self.filter_by_keys(item, spider, ['Name', 'Address', 'Phone'])


    def filter_by_keys(self, item, spider, keys):
        values = [item[key] for key in keys if key in item]
        s = ', '.join(values)

        if s in self.seen:
            raise DropItem("Duplicate {}:".format(s))
        else:
            self.seen.add(s)
            return item
