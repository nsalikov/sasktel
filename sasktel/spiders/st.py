# -*- coding: utf-8 -*-
import json
import scrapy
from scrapy.loader import ItemLoader
from sasktel.items import SasktelItem


class StSpider(scrapy.Spider):
    name = 'st'
    allowed_domains = ['sasktel.com']
    url = 'http://www.sasktel.com/findadealer?lat={lat}&lng={lng}&searchtype=location&sortby=distance&limit=200&sortorder=asc&distance=200&type=SKST&type=SKDL'
    coordinates = [
        ('49.396', '-103.257'),
        ('53.136', '-103.1'),
        ('50.577', '-104.1'),
        ('49.984', '-106.249'),
        ('57.199', '-105.824'),
        ('52.3', '-107.991'),
        ('51.483', '-108.49'),
        ('54.923', '-108.957'),
        ('49.912', '-108.724'),
        ('55.016', '-102.742'),
        ('50.45', '-104.618'),
        ('50.413', '-104.273'),
        ('50.45', '-104.618'),
        ('50.467', '-104.541'),
        ('50.419', '-104.677'),
        ('50.486', '-104.616'),
        ('50.415', '-104.61'),
        ('50.451', '-104.665'),
        ('50.425', '-104.539'),
        ('50.408', '-104.654'),
        ('50.506', '-104.675'),
        ('50.477', '-104.699'),
        ('50.45', '-104.532'),
        ('50.39', '-105.558'),
        ('50.418', '-105.539'),
        ('50.454', '-105.642'),
        ('54.493', '-104.305'),
        ('53.179', '-105.774'),
        ('53.194', '-105.702'),
        ('52.117', '-106.635'),
        ('52.096', '-106.625'),
        ('52.011', '-106.796'),
        ('52.156', '-106.687'),
        ('52.113', '-106.723'),
        ('52.14', '-106.608'),
        ('52.275', '-106.501'),
        ('52.159', '-106.716'),
        ('52.161', '-106.586'),
        ('52.04', '-106.66'),
        ('52.098', '-106.555'),
        ('52.157', '-106.561'),
        ('50.783', '-104.951'),
        ('51.217', '-102.468'),
        ('49.133', '-102.984'),
        ('49.667', '-103.851'),
        ('52.783', '-108.285'),
        ('50.283', '-107.801'),
        ('53.283', '-110.002'),
        ('54.133', '-108.435'),
        ('51.482', '-102.906'),
    ]


    def start_requests(self):
        for (lat, lng) in self.coordinates:
            yield scrapy.Request(self.url.format(lat=lat, lng=lng))


    def parse(self, response):
        try:
            data = json.loads(response.text)
        except:
            return
        if 'data' not in data:
            return

        for store in data['data']:
            l = ItemLoader(item=SasktelItem(), response=response)

            l.add_value('Name', store['name'])
            l.add_value('Address', store['address']['line1'])
            l.add_value('Address', store['address']['line2'])
            l.add_value('City', store['address']['city'])
            l.add_value('Province', 'Saskatchewan')
            l.add_value('Country', 'Canada')
            l.add_value('Phone', store['contacts']['tel'])
            l.add_value('Postal', store['address']['postalCode'])
            l.add_value('Dealer', store['types'])
            l.add_value('Url', store['detailUrl'])

            yield l.load_item()
